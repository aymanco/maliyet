import { Component, OnInit } from '@angular/core';
import { PaymentService } from '../service/payment.service';
import { ProjectService } from '../service/project.service';
import { ComptesService } from '../service/comptes.service';
import { PaymentsService } from '../service/payments.service';
import { TransmutationService } from '../service/transmutation.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-add-payment',
  templateUrl: './add-payment.component.html',
  styleUrls: ['./add-payment.component.css']
})
export class AddPaymentComponent implements OnInit {
  val = 'none';
  val2 = 'none';
  val3 = 'none';
  val4 = 'none';
  payments: any;
  projects: object;
  dofa: any  ;
  dofas: any ;
  valeur = 0;
  tab = Array(0) ;
  rest = null ;
comptes: any ;
num: any ;
  id: string;
  permi: any;
  add: boolean ;
    // tslint:disable-next-line:max-line-length
    constructor(private paymentservice: PaymentService , private transmutationService: TransmutationService , private paymentService: PaymentsService , private project: ProjectService , private compte: ComptesService, private user: UserService) {
      this.id = localStorage.getItem('token');

      this.paymentservice.getpayment().subscribe(data => {
     this.payments = data ;
   });

   this.compte.getComptes().subscribe( data => {
    this.comptes = data ;
   });

   this.project.getproject().subscribe(data => {
     this.projects = data ;
   });
  }
 async ngOnInit() {
    await this.user.getUserByUser(this.id).toPromise().then( data => {
       this.permi = data[0].permi.payments ;
       this.add = this.permi.add ;
      } );
    console.log(this.permi);
  }

  async change(e) {
    this.dofa = null ;
   await this.project.getprojectById(e.target.value).toPromise().then(data => {
      this.dofa = data[0].np ;
      this.dofas = data[0].p ;
    }) ;
    // tslint:disable-next-line:radix
    this.tab = Array(parseInt(this.dofa));
    console.log(this.dofa);
    this.valeur = 0 ;
  }

  cost(e) {
    const x = e.target.value;
  // tslint:disable-next-line:triple-equals
  if ( x == 1 ) { this.valeur = this.dofas.const1 ;  }
  // tslint:disable-next-line:triple-equals
  if ( x == 2 ) { this.valeur = this.dofas.const2 ; }
  // tslint:disable-next-line:triple-equals
  if ( x == 3 ) { this.valeur = this.dofas.const3 ; }
  // tslint:disable-next-line:triple-equals
  if ( x == 4 ) { this.valeur = this.dofas.const4 ; }
  console.log((<HTMLInputElement>document.getElementById('money')).value );

  // tslint:disable-next-line:radix
  const  money = parseInt( (<HTMLInputElement>document.getElementById('money')).value ) ;

  // tslint:disable-next-line:triple-equals
  if ( money  != 0 ) {
  console.log(this.valeur);
  this.Rest1( money , this.valeur );
  }
  }

  Dafea(e) {
    const x = e.target.value ;
    // tslint:disable-next-line:triple-equals
    if ( x == 1  ) {
      this.val = 'block' ;
       this.val2 = 'none' ;
       this.val3 = 'none' ;
        this.val4 = 'none';
      // tslint:disable-next-line:triple-equals
      } else if ( x == 2  ) {
        this.val = 'none' ;
         this.val2 = 'block' ;
          this.val3 = 'none' ;
           this.val4 = 'none' ;
           // tslint:disable-next-line:triple-equals
           } else if ( x == 3  ) {
             this.val = 'none' ;
              this.val2 = 'none' ;
               this.val3 = 'block'
               ; this.val4 = 'none';
               } else {
                  this.val = 'none' ;
                  this.val2 = 'none' ;
                  this.val3 = 'none' ;
                  this.val4 = 'block' ;
                   }
  }

  Rest(e) {
    const solde =  (<HTMLInputElement>document.getElementById('solde')).value ;
    // tslint:disable-next-line:radix
    this.rest = parseInt (solde) - e.target.value ;
  }

  Rest1(e , x ) {
    this.rest =   x - e   ;
  }

  NumB(e) {
    const x = e.target.value ;
    this.compte.getComptesById(x).subscribe( data => {
    this.num = data[0].num ;
    });
    console.log(this.num);
    }

  addpayment(event) {
    const target = event.target ;
    const idp = target.querySelector('#single0').value;
    const numd = target.querySelector('#single1').value;
    const solde = target.querySelector('#solde').value;
    const soldep = target.querySelector('#money').value;
    const idtp = target.querySelector('#payment-type').value;
    const rest = target.querySelector('#rest').value;
    const bank = target.querySelector('#single4').value;
    const numb = target.querySelector('#num').value;
    console.log('hhh : ', idtp );
    console.log(target.querySelector('#paypalm').value);

    // tslint:disable-next-line:triple-equals
    if ( idtp == 3  ) {
    const bankch = target.querySelector('#bank').value;
    const datech = target.querySelector('#date').value;
    const numch = target.querySelector('#numch').value;
    this.paymentService.addpayment( idp , numd , solde , soldep , idtp , rest , bank , numb , bankch , datech ,  numch ).subscribe();

    // tslint:disable-next-line:triple-equals
    } else if ( idtp == 1 ) {
    const paypal = target.querySelector('#paypalm').value;
    this.paymentService.addpayment2( idp , numd , solde , soldep , idtp , rest , bank , numb , paypal  ).subscribe();


    // tslint:disable-next-line:triple-equals
    } else if ( idtp == 2 ) {
    const nomk = target.querySelector('#nomk').value;
    const date = target.querySelector('#datek').value;
    this.paymentService.addpayment3( idp , numd , solde , soldep , idtp , rest , bank , numb , nomk , date  ).subscribe();

    } else {
    const autre = target.querySelector('#autre').value ;
    this.paymentService.addpayment4( idp , numd , solde , soldep , idtp , rest , bank , numb ,  autre ).subscribe();

    }
    // tslint:disable-next-line:triple-equals
    if ( numd == 1  ) { this.dofas.etat1 = true ; this.dofas.date1 = new Date() ; }
    // tslint:disable-next-line:triple-equals
    if ( numd == 2  ) { this.dofas.etat2 = true ; this.dofas.date2 = new Date() ; }
    // tslint:disable-next-line:triple-equals
    if ( numd == 3  ) { this.dofas.etat3 = true ; this.dofas.date3 = new Date() ; }
    // tslint:disable-next-line:triple-equals
    if ( numd == 4  ) { this.dofas.etat4 = true ; this.dofas.date4 = new Date() ; }
    this.project.patchProjec(idp , this.dofas  ).subscribe();





  }

}
