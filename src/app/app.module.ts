import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { FileSelectDirective } from 'ng2-file-upload';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { FilterPipeModule } from 'ngx-filter-pipe';
import {NgxPaginationModule} from 'ngx-pagination';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { BoardComponent } from './board/board.component';


import { environment} from '../environments/environment';
import { AppRoutingModule , routingComponents } from './app-routing.module';
import { AllExpensesComponent } from './all-expenses/all-expenses.component';
import { AddPaymentComponent } from './add-payment/add-payment.component';
import { AllPaymentsComponent } from './all-payments/all-payments.component';
import { AllUsersComponent } from './all-users/all-users.component';
import { AllPsComponent } from './all-ps/all-ps.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { AddServiceComponent } from './add-service/add-service.component';
import { SettingsComponent } from './settings/settings.component';
import { AdduserComponent } from './adduser/adduser.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import { LoginComponent } from './login/login.component';
import { AddClientComponent } from './add-client/add-client.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ServiceDetailsComponent } from './service-details/service-details.component';





const API_URL = environment.apiUrl;

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    BoardComponent,
    routingComponents,
    AllExpensesComponent,
    AddPaymentComponent,
    AllPaymentsComponent,
    AllUsersComponent,
    AllPsComponent,
    AddProjectComponent,
    AddServiceComponent,
    SettingsComponent,
    AdduserComponent,
    LoginComponent,
    AddClientComponent,
    AddEmployeeComponent,
    FileSelectDirective,
    ProjectDetailsComponent,
    ServiceDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatButtonModule, MatCheckboxModule, MatInputModule,
    ReactiveFormsModule , MatNativeDateModule, MatDatepickerModule,
    NgxfUploaderModule , FilterPipeModule , NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [MatButtonModule, MatCheckboxModule]
})
export class AppModule { }
