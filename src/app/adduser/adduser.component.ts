import { Component, OnInit} from '@angular/core';
import { UserService } from '../service/user.service';
import { Permi, Projet, Service, Expense, Payment, Settings, UserC} from '../user.class';
import { HttpClient } from '@angular/common/http';

import { FileUploader } from 'ng2-file-upload';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


const URL = 'http://localhost:200/api/upload';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit  {
  public uploader: FileUploader = new FileUploader({url: URL, itemAlias: 'photo'});

  public projet = new Projet();
  public service = new Service() ;
  public payments = new Expense() ;
  public expenses = new Payment() ;
  public setting = new Settings() ;
  public users = new UserC() ;
  public permi = new Permi();
  URLphoto: string ;
  variable: any;
  form: FormGroup ;
  idd: string;
  permis: any;


  public loadScript1() {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/global/plugins/select2/js/select2.full.min.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);

  }
  public loadScript2() {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);

  }
  public loadScript3() {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/global/plugins/icheck/icheck.min.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);

  }
  public loadScript4() {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/pages/scripts/components-select2.min.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);

  }
  public loadScript5() {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/pages/scripts/form-icheck.min.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);

  }

  constructor(public userService: UserService, private http: HttpClient ,  private formBuilder: FormBuilder, private user: UserService )  {
  this.idd = localStorage.getItem('token');

    this.loadScript1();
    this.loadScript2();
    this.loadScript3();
/*     this.loadScript4();*/
   this.loadScript5();
    }
    async ngOnInit() {
      await this.user.getUserByUser(this.idd).toPromise().then( data => { this.permis = data[0].permi.users ; } );

      this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
      this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log(response) ;
          this.URLphoto = JSON.parse( response ).success  ;
          console.log(this.URLphoto) ;
          this.methode();

      };

      this.form = this.formBuilder.group({
        nom : [null, Validators.required],
        email : [null, Validators.required],
        tel : [null, Validators.required],
        password : [null, Validators.required]
    });
    }
    methode() {
    const target = this.variable.target;
    const username = target.querySelector('#username').value;
    const password = target.querySelector('#password').value;
    const email = target.querySelector('#email').value;
    const tel = target.querySelector('#tel').value;
    let description = target.querySelector('#description').value;
    if (description == null) {
      description = 'null';
    }
    console.log(this.projet);
    const projet1 = target.querySelector('#projet1').checked;
    const projet2 = target.querySelector('#projet2').checked;
    const projet3 = target.querySelector('#projet3').checked;
    const projet4 = target.querySelector('#projet4').checked;
    const projet5 = target.querySelector('#projet5').checked;
    console.log(projet1) ;
    this.projet.add = projet1;
    this.projet.edit = projet2;
    this.projet.deletes = projet3;
    this.projet.poste = projet4;
    this.projet.up = projet5;

    const service1 = target.querySelector('#service1').checked;
    const service2 = target.querySelector('#service2').checked;
    const service3 = target.querySelector('#service3').checked;
    const service4 = target.querySelector('#service4').checked;
    const service5 = target.querySelector('#service5').checked;

    this.service.add = service1;
    this.service.edit = service2;
    this.service.deletes = service3;
    this.service.poste = service4;
    this.service.up = service5;

    const payments1 = target.querySelector('#payments1').checked;
    const payments2 = target.querySelector('#payments2').checked;
    const payments3 = target.querySelector('#payments3').checked;
    const payments4 = target.querySelector('#payments4').checked;
    const payments5 = target.querySelector('#payments5').checked;
    const payments6 = target.querySelector('#payments6').checked;

    this.payments.add = payments1;
    this.payments.deletes = payments2;
    this.payments.edit = payments3;
    this.payments.poste = payments4;
    this.payments.up1 = payments5;
    this.payments.up2 = payments6;

    const expenses1 = target.querySelector('#expenses1').checked;
    const expenses2 = target.querySelector('#expenses2').checked;
    const expenses3 = target.querySelector('#expenses3').checked;
    const expenses4 = target.querySelector('#expenses4').checked;
    const expenses5 = target.querySelector('#expenses5').checked;
    const expenses6 = target.querySelector('#expenses6').checked;

    this.expenses.add = expenses1;
    this.expenses.deletes = expenses2;
    this.expenses.edit = expenses3;
    this.expenses.poste = expenses4;
    this.expenses.up1 = expenses5;
    this.expenses.up2 = expenses6;

    const setting1 = target.querySelector('#setting1').checked;
    const setting2 = target.querySelector('#setting2').checked;
    const setting3 = target.querySelector('#setting3').checked;
    const setting4 = target.querySelector('#setting4').checked;
    const setting5 = target.querySelector('#setting5').checked;
    const setting6 = target.querySelector('#setting6').checked;

    this.setting.addG = setting1;
    this.setting.addp = setting2;
    this.setting.addS = setting3;
    this.setting.edit = setting4;
    this.setting.change = setting5;
    this.setting.add = setting6;


    const users1 = target.querySelector('#users1').checked;
    const users2 = target.querySelector('#users2').checked;
    const users3 = target.querySelector('#users3').checked;
    const users4 = target.querySelector('#users4').checked;
    const users5 = target.querySelector('#users5').checked;
    const users6 = target.querySelector('#users6').checked;
    const users7 = target.querySelector('#users7').checked;
    const users8 = target.querySelector('#users8').checked;
    const users9 = target.querySelector('#users9').checked;

    this.users.addU = users1;
    this.users.addC = users2;
    this.users.addS = users3;
    this.users.editu = users4;
    this.users.editC = users5;
    this.users.editS = users6;
    this.users.deleteU = users7;
    this.users.deleteC = users8;
    this.users.deleteS = users9;

    this.permi.projet = this.projet;
    this.permi.service = this.service;
    this.permi.expenses = this.expenses;
    this.permi.payments = this.payments;
    this.permi.setting = this.setting;
    this.permi.users = this.users;
  // tslint:disable-next-line:max-line-length
  this.userService.addUser(username, tel, email, password, description , this.permi , '../../../uploads/' + this.URLphoto ).subscribe(data => {
    console.log(data);
   });
    }

  async onClickMe(event) {
    this.variable = event ;
    this.methode();
  }


}
