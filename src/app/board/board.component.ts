import { Component, OnInit } from '@angular/core';
import { AddExpenseService } from '../service/addexpense.service';
import { ProjectService } from '../service/project.service';
import { PaymentsService } from '../service/payments.service';
import { ServiceService } from '../service/service.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']

})

export class BoardComponent implements OnInit {
  data: any;
  cost_ex = 0 ;
  cost_pr = 0 ;
  total = 0 ;
  total_nisba = 0 ;
  total_pr = 0 ;
  project: any ;
  payments: any;
  total_irada = 0 ;
  total_py = 0 ;
  total_rest = 0 ;
  nett = 0 ;
  i = 0;
  services: any ;
  x = 0 ;
  total_d = 0;
  y = 0;
public loadScript16() {
  const body = <HTMLDivElement> document.body;
  const script = document.createElement('script');
  script.innerHTML = '';
  script.src = '../../assets/global/scripts/app.min.js';
  script.async = true;
  script.defer = true;
  body.appendChild(script);

}

  // tslint:disable-next-line:max-line-length
  constructor( private  expense: AddExpenseService , private projects: ProjectService , private payment: PaymentsService , private service: ServiceService) {
    }

  async ngOnInit() {
   await this.expense.getExpense().toPromise().then( data => {
     this.data = data ;
   });

   await this.projects.getproject().toPromise().then( data => {
     this.project = data ;
   });

   await this.payment.getpayment().toPromise().then( data => {
   this.payments = data ;
   });


   for ( const indice of this.payments ) {
    // tslint:disable-next-line:radix
    Math.round(  this.total_py += parseInt( indice.solde) );
    // tslint:disable-next-line:radix
    Math.round(  this.total_irada += parseInt( indice.soldep) );
    this.y ++ ;
  }
    // tslint:disable-next-line:radix
  this.total_rest =  this.total_py  - this.total_irada ;

   for ( const indice of this.project ) {
     console.log(indice.np) ;
    // tslint:disable-next-line:radix
    Math.round(  this.total_pr += parseInt(indice.cost)) ;
    // tslint:disable-next-line:radix
    Math.round(this.total_d += parseInt(indice.np) ) ;
    this.i++ ;
   }

   for ( const indice of this.data ) {
     // tslint:disable-next-line:triple-equals
     if ( indice.ps == 1 ) {
       // tslint:disable-next-line:radix
      Math.round( this.cost_pr += parseInt( indice.solde) ) ;
      // tslint:disable-next-line:radix
      Math.round( this.total += parseInt( indice.solde) ) ;
       // tslint:disable-next-line:radix
      Math.round( this.total_nisba += parseInt( indice.total) ) ;
      } else {
       // tslint:disable-next-line:radix
      Math.round( this.cost_ex += parseInt( indice.solde) ) ;
      // tslint:disable-next-line:radix
      Math.round( this.total += parseInt( indice.solde) ) ;
      // tslint:disable-next-line:radix
      Math.round( this.total_nisba += parseInt( indice.total) ) ;
      }
   }
   this.nett = Math.round( this.total_irada - this.total );
  await this.service.getService().toPromise().then( data => {
    this.services = data ;
  });
  for (const indice of this.services ) {
    this.x ++ ;
  }
}
}
