import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { PublicService } from '../service/public.service';
import { AppComponent } from '../app.component';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  id: string;
  url: URL ;
  profil: URL ;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, public authService: AuthService , private user: UserService , private photo: PublicService , private app: AppComponent) {
    this.id = localStorage.getItem('token');
}

  async ngOnInit() {
  this.user.getUserByUser(this.id).toPromise().then( data => {
    this.profil = data[0].url ;
  } );
  this.method();

  }

  logout(): void {
    console.log('Logout');
    this.authService.logout();
    this.router.navigate(['/login']);
    this.id = null ;
    this.app.show = false ;
  }

  async method() {
    await this.photo.getpublic().toPromise().then( data => {
      this.url = data[0].url ;
    });
    console.log(this.url) ;
  }

}
