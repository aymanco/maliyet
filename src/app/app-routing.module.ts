import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddExpenseComponent } from './add-expense/add-expense.component';
import {AllExpensesComponent} from './all-expenses/all-expenses.component';
import {BoardComponent} from './board/board.component';
import {AddPaymentComponent } from './add-payment/add-payment.component';
import { AllPaymentsComponent } from './all-payments/all-payments.component';
import {AllUsersComponent} from './all-users/all-users.component' ;
import {AllPsComponent} from './all-ps/all-ps.component' ;
import {AddProjectComponent} from './add-project/add-project.component' ;
import { AddServiceComponent } from './add-service/add-service.component';
import {SettingsComponent} from './settings/settings.component';
import {AdduserComponent} from './adduser/adduser.component';
import { LoginComponent } from './login/login.component';
import { AddClientComponent } from './add-client/add-client.component';
import {AddEmployeeComponent } from './add-employee/add-employee.component';
import { AuthGuard } from './auth.guard';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ModuleWithProviders } from '@angular/core';
import { ServiceDetailsComponent } from './service-details/service-details.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full' ,
  },

  {path: 'addexpense', component: AddExpenseComponent , canActivate: [AuthGuard]},
  {path: 'allexpenses', component : AllExpensesComponent , canActivate: [AuthGuard]},
  {path: 'board', component : BoardComponent , canActivate: [AuthGuard]},
  {path: 'addpayment', component: AddPaymentComponent, canActivate: [AuthGuard]} ,
  {path: 'allpayments', component: AllPaymentsComponent, canActivate: [AuthGuard]},
  {path: 'allusers', component: AllUsersComponent, canActivate: [AuthGuard]},
  {path: 'allps', component: AllPsComponent, canActivate: [AuthGuard]},
  {path: 'addproject', component: AddProjectComponent, canActivate: [AuthGuard]},
  {path: 'addservice', component: AddServiceComponent, canActivate: [AuthGuard]},
  {path: 'settings', component: SettingsComponent, canActivate: [AuthGuard]},
  {path: 'adduser' , component:  AdduserComponent, canActivate: [AuthGuard]},
  {path: 'login' , component: LoginComponent},
  {path: 'addclient' , component: AddClientComponent, canActivate: [AuthGuard]},
  {path: 'addemployee' , component: AddEmployeeComponent, canActivate: [AuthGuard]},
  {path: 'projectdetails/:id' , component: ProjectDetailsComponent, canActivate: [AuthGuard]} ,
  {path: 'servicedetails/:id' , component: ServiceDetailsComponent, canActivate: [AuthGuard]}
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
export const routingModule: ModuleWithProviders = RouterModule.forRoot(routes);
export const routingComponents = [ AdduserComponent , SettingsComponent, AddServiceComponent,
  AddProjectComponent, AddExpenseComponent, AllPsComponent, AllUsersComponent , AllExpensesComponent ,
   BoardComponent , AddPaymentComponent, AllPaymentsComponent, LoginComponent, AddClientComponent, AddEmployeeComponent ];
