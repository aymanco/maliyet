import { Cost } from './cost.model';

export class Costs implements Cost {
  date1: Date;
  date2: Date;
  date3: Date;
  date4: Date;
  etat1: boolean;
  etat2: boolean;
  etat3: boolean;
  etat4: boolean;
  dconst1: Date;
  dconst2: Date;
  dconst3: Date;
  dconst4: Date;
  const1: Number;
  const2: Number;
  const3: Number;
  const4: Number;
}
