import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../service/employee.service' ;
import {TacheService} from '../service/tache.service';
import {Paypal, Bank , Autre } from '../Tranfert.class';
import { FormArray } from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { UserService } from '../service/user.service';


@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  add: boolean;
  public go  = 'none';
  go2 = 'none' ;
  go3 = 'none' ;
  taches: object  ;
  employees: any ;
  paypal = new  Paypal() ;
  paypalt: Paypal[] ;
  i = 0;
 bank = new Bank();
 autre = new Autre();
  idd: string;
  permi: any;

  public loadScript18() {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/pages/scripts/components-select2.min.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);
  }
  public loadScript1() {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/global/plugins/select2/js/select2.full.min.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);
  }
   public loadScript12() {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/pages/scripts/components-date-time-pickers.min.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);
  }
  public loadScript13() {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);
  }
  public loadScript14() {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/global/plugins/counterup/jquery.counterup.min.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);
  }
  methode(event) {
    const target = event.target;
    const select = target.value;
    // tslint:disable-next-line:triple-equals
    if ( select == 1 ) {this.go = 'block'; this.go2 = 'none';  this.go3 = 'none' ;
    }
    // tslint:disable-next-line:triple-equals
    if ( select == 2 ) {this.go = 'none';  this.go2 = 'block'; this.go3 = 'none' ;
  }
    // tslint:disable-next-line:triple-equals
    if ( select == 3 ) {this.go = 'none';  this.go2 = 'none';  this.go3 = 'block' ; }
    // tslint:disable-next-line:triple-equals
    if ( select == 0 ) {this.go = 'none';  this.go2 = 'none';  this.go3 = 'none' ; }
    console.log(this.go );
  }

  addPaypal(event) {
    const target = event.target ;
    this.paypalt  = target.querySelector('#emailt').value ;
    console.log(this.paypalt);
  }

  onClickMe(event) {
    console.log('ok');
    const target = event.target;
    const nom = target.querySelector('#nom').value;
    const tel = target.querySelector('#tel').value;
    const email = target.querySelector('#email').value;
    const ids = target.querySelector('#single1').value;
    const desc = target.querySelector('#desc').value;
    const transfert = target.querySelector('#transfert').value ;
    const emailt = target.querySelector('#emailt').value;
    const bank = target.querySelector('#bank').value;
    const num = target.querySelector('#num').value;
    const autre = target.querySelector('#autre').value;
    const num2 = target.querySelector('#num2').value;

    // tslint:disable-next-line:triple-equals
    if ( transfert == 1 ) {
      console.log('11111') ;
     /*  this.employeeService.getEmployee().subscribe(data => {
        this.last = data ;
        console.log(this.last);
      }); */
/*     this.employeeService.addEmployee1(nom, tel, email, ids, desc, transfert ).subscribe();
 */

  }
  }
  constructor(public employeeService: EmployeeService, public tacheService: TacheService , private user: UserService) {

  this.idd = localStorage.getItem('token');

    this.loadScript18();
    this.loadScript1();
    this.loadScript12();
    this.loadScript13();
    this.loadScript14();
    this.tacheService.getTache().subscribe(data => {
      this.taches = data ;
    });
    this.employeeService.getEmployee().subscribe();

  }
  async ngOnInit() {

    await this.user.getUserByUser(this.idd).toPromise().then( data => { this.permi = data[0].permi.users ; } );
    this.add = this.permi.addS ;
  }

}
