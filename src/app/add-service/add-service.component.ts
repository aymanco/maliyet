import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service/service.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-add-service',
  templateUrl: './add-service.component.html',
  styleUrls: ['./add-service.component.css']
})
export class AddServiceComponent implements OnInit {
  form: FormGroup ;
  id: string;
  permi: any;
  add: boolean ;

  constructor(private serviceService: ServiceService , private formBuilder: FormBuilder, private user: UserService) {
    this.id = localStorage.getItem('token');

   }

  async ngOnInit() {

    await this.user.getUserByUser(this.id).toPromise().then( data => {
       this.permi = data[0].permi.service ;
       this.add = this.permi.add ;
      } );

    this.form = this.formBuilder.group({
      nom : [null, Validators.required]
  });

  }
  click(e) {
    const target = e.target ;
    const nom =  target.querySelector('#nom').value ;
    const solde = target.querySelector('#solde').value;
    const dated = target.querySelector('#dated').value ;
    const datef = target.querySelector('#datef').value ;
    const desc = target.querySelector('#desc').value ;
    const req = target.querySelector('#req').value ;
    this.serviceService.addproject( nom  , dated , datef , solde , desc , req  ).subscribe(data => {
      console.log(data);
    });
    console.log('jjj');
  }

}
