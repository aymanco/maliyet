import { Component, OnInit } from '@angular/core';
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { PublicService } from '../service/public.service';
import { TacheService } from '../service/tache.service';
import { ExpenseService } from '../service/expense.service';
import { PaymentService } from '../service/payment.service';
import { delay } from 'q';
import { ComptesService } from '../service/comptes.service';
import { AppComponent } from '../app.component';
import { TransmutationService } from '../service/transmutation.service';
import { HeaderComponent } from '../header/header.component';
import { UserService } from '../service/user.service';



const URL = 'http://localhost:200/api/upload';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit  {
  public uploader: FileUploader = new FileUploader({url: URL, itemAlias: 'photo'});
  URLphoto: any ;
selectedFile: File = null;
 fd = new FormData();
 taches: any ;
 expenses: any ;
 payments: any ;
 comptes: any ;
 percentages: any ;
 valeur  = 0 ;
 test = false ;
 test1 = false ;
p: any ;
x: any ;
compte1: any ;
compte2: any ;

trans: any ;

nisab: any ;
id: any = 0  ;
show: any ;
open = false  ;
  idd: string;
  permi: any;
  addp: boolean ;
  add: boolean ;
  addG: boolean ;
  addS: boolean ;
  change: boolean ;
  titre: string ;
  // tslint:disable-next-line:max-line-length
  constructor( private app: AppComponent ,  private transmutationService: TransmutationService ,  private comptesService: ComptesService , private publicService: PublicService , private tachservice: TacheService, private expenseservice: ExpenseService, private paymentservice: PaymentService, private user: UserService ) {
  this.idd = localStorage.getItem('token');
  this.titre = 'عام' ;

this.tachservice.getTache().subscribe(data => {
  this.taches = data ;
  console.log(this.taches);
});

this.comptesService.getPercentage().subscribe( data => {
this.nisab = data ;
});



this.transmutationService.getTrans().subscribe( data => {
  this.trans = data ;
});

this.expenseservice.getexpense().subscribe( data => {
  this.expenses = data ;

});

this.paymentservice.getpayment().subscribe(data => {
  this.payments = data ;
});

this.comptesService.getComptes().subscribe( data => {
  this.comptes = data ;
});

this.comptesService.getPercentage().subscribe( data => {
  this.percentages = data ;
});
}

async ngOnInit() {
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
     this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
       console.log(response) ;
         this.URLphoto = JSON.parse( response ) ;
         this.image();

     };

     await this.user.getUserByUser(this.idd).toPromise().then( data => {
       this.permi = data[0].permi.setting ;
       this.addp = this.permi.addp ;
       this.add = this.permi.add ;
       this.addG = this.permi.addG ;
       this.addS = this.permi.addS ;
       this.change = this.permi.change ;
      } );

    }
    Titre(e) {
      // tslint:disable-next-line:triple-equals
      if (e == 0) { this.titre = 'عام' ;
    // tslint:disable-next-line:triple-equals
    } else if ( e == 1  ) {this.titre = 'حسابات الشركة' ;
    // tslint:disable-next-line:triple-equals
    } else if ( e == 2 ) {this.titre = 'التحويلات' ;
    // tslint:disable-next-line:triple-equals
    } else if (e == 3 ) {this.titre = 'النسب' ;
    // tslint:disable-next-line:triple-equals
    } else if (e == 4) {this.titre = 'تقارير النسب' ; }

    }
    async image () {
      console.log(this.URLphoto.success) ;
      await  this.publicService.putphoto( 0 ,  '../../../uploads/' + this.URLphoto.success ).toPromise();

   }


  async click(event ) {
  const target = event.target ;
  const nom = target.querySelector('#nom').value ;
  await  this.publicService.putpublic( 0 , nom ).toPromise();
  this.app.methode();
 }
 async addService() {
   const element = (<HTMLInputElement> document.getElementById('nomtache')).value ;
   await this.tachservice.addTache(element).toPromise() ;
    this.tachservice.getTache().subscribe(data => {
     this.taches = data ;
   });
 }

 async addExpense() {
   const element = (<HTMLInputElement>document.getElementById('nomex')).value ;
  await this.expenseservice.addexpense(element).toPromise();
   this.expenseservice.getexpense().subscribe( data => {
     this.expenses = data ;
   });
 }
  async addPayment() {
  const element = (<HTMLInputElement>document.getElementById('nomp')).value ;
   await this.paymentservice.addpayment(element).toPromise();
  this.paymentservice.getpayment().subscribe( data => {
    this.payments = data ;
  });

}

async bank(event) {

console.log('hi ! ');
const target = event.target;
const nom = target.querySelector('#nom').value ;
const num = target.querySelector('#num').value ;
const solde = target.querySelector('#solde').value ;
const iban = target.querySelector('#iban').value ;
const percentage = target.querySelector('#percentage').value ;
const nomperc = target.querySelector('#nomperc').value ;
await this.comptesService.addComptes(nom, num , solde , iban , percentage, nomperc).toPromise() ;
this.comptesService.getComptes().subscribe( data => {
this.comptes = data ;
});

}

  async method1() {
  this.test = true;
  const solde = (<HTMLInputElement>document.getElementById('soldes')).value ;
  const percn = (<HTMLSelectElement>document.getElementById('percs')).value ;
 if ( percn === '0' ) { this.test = false; }
 console.log(this.test);
  // tslint:disable-next-line:radix
 await this.comptesService.getPercentageById(parseInt(percn)).toPromise().then(data => {
     this.p = data ;
     if ( percn !== '0' ) { this.x = this.p[0].perc ; }
  });
 if ( this.test === true && this.test1 === true  ) {
// tslint:disable-next-line:radix
this.valeur = parseInt(solde) - ( parseInt(solde) * parseInt( this.x ) / 100 ) ;
// tslint:disable-next-line:radix
} else {this.valeur = parseInt(solde)  ; }

}



method2() {
  this.test1 = true;
  const solde = (<HTMLInputElement>document.getElementById('soldes')).value ;
  const perc = (<HTMLSelectElement>document.getElementById('percs')).value ;
 if ( this.test === true && this.test1 === true  ) {
// tslint:disable-next-line:radix
this.valeur = parseInt(solde) - ( parseInt(solde) * parseInt( this.x ) / 100 ) ;
 // tslint:disable-next-line:radix
 } else {this.valeur = parseInt(solde)  ; }
}


  async transfert(event) {
const target = event.target ;
console.log('ti worked ');
 const tran1 = target.querySelector('#compte1').value;
const tran2 = target.querySelector('#compte2').value;
const solde = target.querySelector('#soldes').value;
const select = target.querySelector('#percs').value;
const nett = target.querySelector('#nett').value;

 // tslint:disable-next-line:radix
await this.comptesService.getComptesById(parseInt(tran1)).toPromise().then(data => {
this.compte1 = data;
});

console.log(' compte 1 ', this.compte1);
// tslint:disable-next-line:radix
this.compte1[0].solde = parseInt(this.compte1[0].solde) - parseInt(nett) ;
console.log('compte 1 nv ', this.compte1);


// tslint:disable-next-line:radix
await this.comptesService.getComptesById(parseInt(tran2)).toPromise().then(data => {
  this.compte2 = data;

});
console.log(' compte 2 ', this.compte2);
// tslint:disable-next-line:radix
this.compte2[0].solde = parseInt(this.compte2[0].solde) + parseInt( nett );
console.log(' compte 2 nv ', this.compte2);

await this.comptesService.putComptes(tran1 , this.compte1[0].solde ).toPromise();

await this.comptesService.putComptes(tran2, this.compte2[0].solde  ).toPromise();

await this.comptesService.getComptes().toPromise().then( data => {
  this.comptes = data ;
});

this.transmutationService.addTrans(
  this.compte1[0].nom,
   this.compte1[0].num,
    this.compte2[0].nom ,
    this.compte2[0].num,
     solde - nett,
     solde,
     Date.now()
  )
  .subscribe();
  await delay(500) ;
  this.transmutationService.getTrans().subscribe( data => {
    this.trans = data ;
  });

}


  async addPercentage(event) {
  const target = event.target ;
  const nom = target.querySelector('#nom').value ;
  const valeur = target.querySelector('#valeur').value ;
  const req = target.querySelector('#req').value ;
  await  this.comptesService.addPercentage(nom , valeur , req).toPromise();
  this.comptesService.getPercentage().subscribe( data => {
    this.nisab = data;
  });
}
  async index(e) {
this.id = e ;
console.log(this.id);
await this.comptesService.getPercentageById(this.id).toPromise().then( data => {
  this.show = data[0] ;
  console.log(' show ', this.show);
});
this.open = true ;
}
  async deletPercentage() {
  await this.comptesService.deletPercentageById(this.id).toPromise();

  await this.comptesService.getPercentage().toPromise().then( data => {
    this.nisab = data;
  });

}


}
