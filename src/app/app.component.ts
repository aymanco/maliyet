import { Component, Input } from '@angular/core';
import { PublicService } from './service/public.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent {
 private  public1:  any ;

   title: string   ;
   show = false ;
  constructor(private publicService: PublicService ) {
   this.methode();
  }
  methode() {

    this.publicService.getpublic().subscribe(data => {
      this.public1 = data ;
   this.title = this.public1[0].nom ;
   });

  }

}
