import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from '../service/project.service';
import { ClientService } from '../service/client.service';
import { PaymentsService } from '../service/payments.service';
import { AddExpenseService } from '../service/addexpense.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {
  projects: any ;
  id: number ;
  clients: any;
  tabC = Array(0) ;
  tabD = Array(0) ;
  tabT = Array(0) ;
  tabE = Array(0) ;
  p: any;
  payments: any ;
  total1 = 0;
  total2 = 0;
  total3 = 0;
  total4 = 0;
  date1: any ;
  date2: string  ;
  date3: any ;
  date4: any ;
  date = [] ;
  expenses: any;
  nett = 0 ;
  solde1: number ;
  solde2: number ;
  solde3: number ;
  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute , private project: ProjectService , private client: ClientService , private payment: PaymentsService, private expense: AddExpenseService ) {
    this.method() ;
  }

  async ngOnInit() {

  }

   async method() {


    this.route.params.subscribe( params  => {
      this.id = params.id ;
    });

    await this.project.getprojectById(this.id).toPromise().then( data => {
      this.projects = data[0] ;
      this.p = data[0].p ;
    });

    await this.client.getCLientById(this.projects.idc).toPromise().then( data => {
      this.clients = data[0] ;
    });

    await this.payment.getpaymentByIdp(this.id).toPromise().then( data => {
      this.payments = data ;
    } );

    for ( const total of this.payments ) {
      // tslint:disable-next-line:triple-equals
      if ( total.numd == 1 ) {
       // tslint:disable-next-line:radix
       Math.round( this.total1 += parseInt( total.soldep ) );
      // tslint:disable-next-line:triple-equals
      } else if (total.numd == 2 ) {
        // tslint:disable-next-line:radix
        Math.round( this.total2 += parseInt(total.soldep) );
      // tslint:disable-next-line:triple-equals
      } else if (total.numd == 3 ) {
        // tslint:disable-next-line:radix
        Math.round( this.total3 += parseInt( total.soldep)) ;
      // tslint:disable-next-line:triple-equals
      } else if ( total.numd == 4 ) {
        // tslint:disable-next-line:radix
        Math.round(  this.total4 += parseInt( total.soldep ));
       }

    }

    // tslint:disable-next-line:radix
    this.tabC = Array(parseInt(this.projects.np)) ;
    // tslint:disable-next-line:radix
    this.tabD = Array(parseInt(this.projects.np)) ;
     // tslint:disable-next-line:radix
     this.tabT = Array(parseInt(this.projects.np)) ;
      // tslint:disable-next-line:radix
      this.tabE = Array(parseInt(this.projects.np)) ;
    if ( this.projects.np >= 1 ) {
      console.log('hi');
      console.log(this.p);
      this.tabC[0] = this.p.const1 ;
       this.tabD[0] = this.p.dconst1 ;
       // tslint:disable-next-line:triple-equals
       if ( this.total1 >= this.p.const1  ) {
         this.tabT[0] = 'done' ;
         this.tabE[0] = 'check' ;
       // tslint:disable-next-line:triple-equals
       } else if ( new Date() > new Date(this.p.dconst1)  ) {
        this.tabT[0] = 'late' ;
        this.tabE[0] = 'close' ;
       } else { this.tabT[0] = 'wait' ;
       this.tabE[0] = 'close' ;
      }
       }
    if ( this.projects.np >= 2  ) {
      this.tabC[1] = this.p.const2 ;
      this.tabD[1] = this.p.dconst2 ;
      this.date2 = this.p.dconst2 ;
        // tslint:disable-next-line:triple-equals
        if ( this.total2 >= this.p.const2) {
          this.tabT[1] = 'done' ;
          this.tabE[1] = 'check' ;
        // tslint:disable-next-line:triple-equals
        } else if (new Date() > new Date( this.p.dconst2) ) {
         this.tabT[1] = 'late' ;
         this.tabE[1] = 'close' ;
        } else { this.tabT[1] = 'wait' ;
        this.tabE[1] = 'close' ;
       }
    }
    if ( this.projects.np >= 3  ) {
      this.tabC[2] = this.p.const3;
      this.tabD[2] = this.p.dconst3 ;
        // tslint:disable-next-line:triple-equals
        if ( this.total3 >= this.p.const3   ) {
          this.tabT[2] = 'done' ;
          this.tabE[2] = 'check' ;
        // tslint:disable-next-line:triple-equals
        } else if ( new Date() > new Date( this.p.dconst3 )) {
         this.tabT[2] = 'late' ;
         this.tabE[2] = 'close' ;
        } else { this.tabT[2] = 'wait' ;
        this.tabE[2] = 'close' ;
       }
    }
    if ( this.projects.np >= 4 ) {
      this.tabC[3] = this.p.const4;
      this.tabD[3] = this.p.dconst4 ;
        // tslint:disable-next-line:triple-equals
        if ( this.total4 >= this.p.const4  ) {
          this.tabT[3] = 'done' ;
          this.tabE[3] = 'check' ;
        // tslint:disable-next-line:triple-equals
        } else if ( new Date() > new Date( this.p.dconst4) ) {
         this.tabT[3] = 'late' ;
         this.tabE[3] = 'close' ;
        } else { this.tabT[3] = 'wait' ;
        this.tabE[3] = 'close' ;
       }
    }

    await this.payment.getpaymentByIdp(this.id).toPromise().then( data => {
      this.payments = data ;
    } );
    await this.expense.getExpenseByIdpsIdp( 1 , this.id ).toPromise().then( data => {
    this.expenses = data ;
    });
    console.log(this.expenses);
    for ( const expense of this.expenses ) {
      // tslint:disable-next-line:radix
     Math.round( this.nett += parseInt(expense.solde)) ;
    }
    console.log(this.nett) ;
    this.solde1 = Math.round(this.total1 + this.total2 + this.total3 + this.total4 ) ;
    this.solde2 = this.projects.cost - this.solde1 ;
    this.solde3 = this.nett ;




  }

}
