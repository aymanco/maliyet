import { Component,  OnInit } from '@angular/core';
import {UserService} from '../service/user.service';
import { ClientService } from '../service/client.service';
import { EmployeeService } from '../service/employee.service';


@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.css']
})
export class AllUsersComponent implements OnInit  {
 users: any ;
 userFilter: any = { userName: '' };
clientFilter: any = { nom: '' };
empolyeFiltr: any = { nom: ''};
 clients: any ;
employes: any ;
u = 0 ;
e = 0 ;
c = 0 ;
  constructor(private  userService: UserService , private client: ClientService , private employer: EmployeeService) { }


async ngOnInit() {
  await this.userService.getUser().toPromise().then(data => {
    this.users = data;
    this.u = this.users.length ;
    console.log('user ', this.users);
 });

 await this.client.getCLient().toPromise().then( data => {
   this.clients = data ;
   this.c = this.clients.length ;

 });
 console.log(this.clients);

 await this.employer.getEmployee().toPromise().then(data => {this.employes = data ;
  this.e = this.employes.length ;
} );


}
  async mos() {
  // tslint:disable-next-line:triple-equals
  if (( <HTMLInputElement>document.getElementById('checkbox-2')).checked == false ) { this.users = null; this.u = 0 ;
  } else {await this.userService.getUser().toPromise().then(data => {this.users = data;
    this.u = this.users.length ;
   }); }
}

async em() {
  // tslint:disable-next-line:triple-equals
  if ((<HTMLInputElement>document.getElementById('checkbox-3')).checked == false ) { this.employes = null; this.e = 0 ;
  } else { await this.employer.getEmployee().toPromise().then(data => {this.employes = data ;
    this.e = this.employes.length ;
  } ); }

}

  async cl() {
  // tslint:disable-next-line:triple-equals
  if (( <HTMLInputElement>document.getElementById('checkbox-4')).checked == false ) { this.clients = null; this.c = 0 ;
  } else { await this.client.getCLient().toPromise().then( data => {this.clients = data ;
    this.c = this.clients.length ;
  }); }

}
  async all(e) {
  // tslint:disable-next-line:triple-equals
  if (e.target.checked == true ) {
    await this.userService.getUser().toPromise().then(data => {this.users = data; this.u = this.users.length ; } );
    await this.employer.getEmployee().toPromise().then(data => {this.employes = data ; this.e = this.employes.length ; } );
    await this.client.getCLient().toPromise().then( data => {this.clients = data ; this.c = this.clients.length ;  });
    } else {
      this.mos() ;
      this.em();
      this.cl();
    }
}

}
