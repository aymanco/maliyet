import {Transfert1, Transfert2, Transfert3} from './transfert.model';

export class Paypal implements Transfert1 {
  mail: String;
}
export class Bank implements Transfert2 {
  nom: String;
  num: number;
}
export class Autre implements Transfert3 {
  nom: String;
   num: number;
}
