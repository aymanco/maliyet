import { Component, OnInit } from '@angular/core';
import { ExpenseService } from '../service/expense.service';
import { AddExpenseService } from '../service/addexpense.service';
import { ProjectService } from '../service/project.service';
import { ServiceService } from '../service/service.service';
import { EmployeeService } from '../service/employee.service';
import { ComptesService } from '../service/comptes.service';
import { PaymentService } from '../service/payment.service';
import { FormControl } from '@angular/forms';
import * as jsPDF from 'jspdf' ;
import * as html2canvas from 'html2canvas';
import { delay } from 'q';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-all-expenses',
  templateUrl: './all-expenses.component.html',
  styleUrls: ['./all-expenses.component.css']
})
export class AllExpensesComponent implements OnInit {
  date = new FormControl(new Date());
expenses: any;
expenses2: any;
i = 0 ;
tab = [] ;
etat = [] ;
amil = [] ;
num = 10 ;
id: number ;
masroufa: any ;
typeM: any ;
psM: any ;
employeeM: any;
bankM: any;
transfertM: any;
percentageM: any;
open = false ;
open1 = false ;
typex: any ;
projects: any;
services: any;
employees: any ;
comptes: any ;
payments: any ;
yes1 = false ;
yes2 = false ;
show: any ;
nisba: string ;
total: number = null ;
persentages: any ;
addsolde: any;
patch: any;
modif = false ;
  idd: string;
  permi: any;

  // tslint:disable-next-line:max-line-length
  constructor( private transfert: PaymentService ,  private expense: AddExpenseService, private compte: ComptesService , private project: ProjectService, private service: ServiceService , private type: ExpenseService , private employe: EmployeeService , private user: UserService) {
  this.idd = localStorage.getItem('token');

   }

  async ngOnInit() {
    await this.user.getUserByUser(this.idd).toPromise().then( data => { this.permi = data[0].permi.expenses ; } );

    await this.compte.getPercentage().toPromise().then( data => {
      this.persentages = data ;
      });

    await this.compte.getComptes().toPromise().then( data => {
      this.comptes = data ;
      });
      await this.transfert.getpayment().toPromise().then( data => {
        this.payments = data ;
         });
    this.methode() ;

}

  async methode() {
    this.i = 0;

  await this.expense.getExpense().toPromise().then(data => {
    this.expenses = data ;
  }
  );

  for ( const allExpenses of this.expenses ) {

    this.i++ ;
    // tslint:disable-next-line:triple-equals
    if ( allExpenses.ps == 1  ) {
        await this.project.getprojectById(allExpenses.idps).toPromise().then(data => {
          this.tab[this.i - 1] = data[0].nom ;
        });
    } else {
      await  this.service.getServiceByID(allExpenses.idps).toPromise().then( data => {
          this.tab[this.i - 1 ] = data[0].nom ;
      });
    }
    await this.type.getExpenseById(allExpenses.ide).toPromise().then( data => {
      this.etat[this.i - 1 ] = data[0].nom ;
    });
    await this.employe.getEmployeeById(allExpenses.idem).toPromise().then( data => {
    this.amil[this.i - 1] = data[0].nom ;
    });


}
}

deletById (val) {
this.id = val ;


}

async delete(  ) {
await  this.expense.deletExpenses(this.id).toPromise().then() ;
this.methode() ;
}

  async affiche( x ) {
await this.expense.getExpenseById(x).toPromise().then( data => {
this.masroufa = data[0] ;
}) ;
await this.type.getExpenseById(this.masroufa.ide).toPromise().then( data1 => {
  this.typeM = data1[0] ;
});

// tslint:disable-next-line:triple-equals
if ( this.masroufa.ps == 1  ) {
await this.project.getprojectById(  this.masroufa.idps  ).toPromise().then( data2 => {
this.psM = data2[0] ;
});
} else {
  await this.service.getServiceByID( this.masroufa.idps ).toPromise().then( data3 => {
  this.psM = data3[0] ;
} );
}

await this.employe.getEmployeeById( this.masroufa.idem ).toPromise().then( data4 => {
this.employeeM = data4[0] ;
});

await this.compte.getComptesById( this.masroufa.idb ).toPromise().then( data5 => {
this.bankM = data5[0] ;
} );

await this.transfert.getpaymentById(this.masroufa.idt).toPromise().then( data6 => {
this.transfertM = data6[0] ;
} );

await this.compte.getPercentageById(this.masroufa.idp).toPromise().then( data7 => {
this.percentageM = data7[0] ;
} );
this.open = true ;
}

  async edit(x) {


    this.total = x.total ;
this.patch = x ;

    this.affiche(x.id);
  await this.expense.getExpenseById(x.id).toPromise().then( data => {
    this.typex = data[0] ;
  } );
  await this.type.getexpense().toPromise().then( data => {
this.expenses2 = data ;
  }) ;

  await this.employe.getEmployee().toPromise().then( data => {
    this.employees = data ;
      }) ;

  await this.project.getproject().toPromise().then( data => {
    this.projects = data ;
  });

  await this.service.getService().toPromise().then( data => {
    this.services = data ;
  });
  this.modif = true ;
}


async Compte(e) {
  const id = e.target.value ;
  await this.employe.getEmployeeById(id).toPromise().then( data => {
  this.show = data[0] ;
  });
  this.open1 = true ;
}

async solde1 ( e ) {
  this.yes1 = true;
  // tslint:disable-next-line:triple-equals
  if (this.yes2 == true ) {
  const cost = e.target.value ;
  const id =  (<HTMLInputElement>document.getElementById('single10')).value ;
  await this.compte.getPercentageById( id ).toPromise().then( data => {
    this.nisba = data[0].perc;
  } );

  // tslint:disable-next-line:radix
  this.total = parseInt(cost) +  (parseInt(cost) * parseInt(this.nisba ) / 100 );
  }
}

  async solde( e ) {
    this.yes2 = true ;

    // tslint:disable-next-line:triple-equals
    if ( this.yes1 == true) {
  const id = e.target.value ;
  await this.compte.getPercentageById( id ).toPromise().then( data => {
    this.nisba = data[0].perc;
  } );

  const cost = (<HTMLInputElement>document.getElementById('single11')).value  ;
  // tslint:disable-next-line:radix
  this.total = parseInt(cost) +  (parseInt(cost) * parseInt(this.nisba ) / 100 );
    }
}

async add(event) {
 await this.affiche(this.patch.id).then() ;
  const target = event.target ;

const nom = target.querySelector('#nom').value ;
const ide = target.querySelector('#single1').value ;
const desc = target.querySelector('#desc').value ;
const ps = target.querySelector('#single0').value ;
const idem = target.querySelector('#single2').value ;
const idb = target.querySelector('#single33').value ;
const idt = target.querySelector('#single3').value ;
const solde = target.querySelector('#single11').value ;
const idp = target.querySelector('#single10').value ;
const total = target.querySelector('#total').value ;
const date = target.querySelector('#date').value ;
const req = target.querySelector('#req').value ;

this.id = ps % 10 ;

 // tslint:disable-next-line:max-line-length
 this.expense.patchExpenses( this.patch.id , nom , ide , desc , this.id , Math.round( ps / 10), idem , idb , idt , solde , idp , total , date , req ).subscribe();

await this.compte.getComptesById(idb).toPromise().then( data => {
  this.addsolde = data[0].solde ;
});

 // tslint:disable-next-line:radix
  await  this.compte.putComptes( this.patch.idb , Math.round(parseInt( this.addsolde) + parseInt(this.patch.total) ) ).toPromise().then();
 // tslint:disable-next-line:radix
 this.compte.putComptes( idb , Math.round(parseInt( this.addsolde) - parseInt(total) ) ).subscribe();

 }





next() {
this.num += 10 ;
}

async downloadPDF(id) {
  await this.affiche(id);
  await delay(1000) ;
  this.open = true ;
  const data = document.getElementById('show');
  html2canvas(data).then(canvas => {
    // Few necessary setting options
    const imgWidth = 208;
    const imgHeight = canvas.height * imgWidth / canvas.width;
    const contentDataURL = canvas.toDataURL('image/png');
    const pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
    const position = 0;
    pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth + 30, imgHeight );
    pdf.save('MYPdf.pdf'); // Generated PDF
  });

}
}
