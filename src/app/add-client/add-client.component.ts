import { Component, OnInit } from '@angular/core';
import {UserService} from '../service/user.service';
import { ClientService } from '../service/client.service';


@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {
   payment = UserService[this.payment]  ;
  idd: string;
  permi: any;
  add: boolean ;
  // tslint:disable-next-line:max-line-length
  constructor(public userService: UserService, public clientservise: ClientService, private user: UserService) {
  this.idd = localStorage.getItem('token');


  }

  onClickMe(event) {
    console.log(event);
    const target = event.target;
    const nom = target.querySelector('#nom').value;
    const select = target.querySelector('#select').value;
    const desc = target.querySelector('#desc').value;
  this.clientservise.addClient(select, nom,  desc).subscribe();
  }
  async ngOnInit() {
    await this.user.getUserByUser(this.idd).toPromise().then( data => { this.permi = data[0].permi.users ; } );
    console.log(this.permi);
    this.add = this.permi.addC ;

    this.userService.getpayment().subscribe(data => {
      this.payment = data;
   });


  }


}
