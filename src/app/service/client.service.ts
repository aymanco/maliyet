import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' ;
@Injectable({providedIn: 'root'})

export class ClientService {
  constructor (private http: HttpClient) {}

    getCLient() {
        return this.http.get('http://localhost:3000/client');
         }
         getCLientById(id: number ) {
          return this.http.get('http://localhost:3000/client?id=' + id );
           }
    addClient( idp: number , nom: String , dsecription: String ) {
      return this.http.post('http://localhost:3000/client', {
        idp: idp,
        nom: nom,
        dsecription: dsecription
        }) ;
    }

}
