import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' ;

@Injectable({providedIn: 'root'})

export class PublicService {
  constructor (private http: HttpClient ) {}
  private headers = new Headers ({ 'Content-Type': 'application/json' }) ;

    getpublic() {
        return this.http.get('http://localhost:3000/public');
         }
    putpublic( id: number , nom: String  ) {
      return this.http.patch('http://localhost:3000/public/' + id , {nom: nom  }  );

    }
    putphoto( id: number , url: String  ) {
      return this.http.patch('http://localhost:3000/public/' + id , {url: url  }  );

    }

}
