import { Permis} from '../user.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' ;
import { Url } from 'url';
@Injectable({providedIn: 'root'})

export class UserService {
  constructor (private http: HttpClient , ) {}

    getUser() {
        return this.http.get('http://localhost:3000/users');
         }
         getUserByUser( user: string ) {
          return this.http.get('http://localhost:3000/users?userName=' + user);
           }
         getpayment() {
          return this.http.get('http://localhost:3000/payment');
           }
    addUser( userName: String, tel: string, emial: String, password: String, description: String , permi: Permis , url: string ) {
      return this.http.post('http://localhost:3000/users', {
        userName: userName,
        emial: emial,
        password: password,
        description: description,
        permi: permi ,
        url: url
        }) ;
    }



}
