import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' ;
@Injectable({providedIn: 'root'})

export class TacheService {
  constructor (private http: HttpClient , ) {}

    getTache() {
        return this.http.get('http://localhost:3000/tache');
         }
    addTache( nom: String ) {
      return this.http.post('http://localhost:3000/tache', {
        nom: nom
        }) ;
    }

}
