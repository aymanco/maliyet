import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' ;
@Injectable({providedIn: 'root'})

export class AddExpenseService {
  constructor (private http: HttpClient) {}

    getExpense() {
        return this.http.get('http://localhost:3000/expenses');
         }
         getExpenseByIdpsIdp(id: number , idps: number  ) {
          return this.http.get('http://localhost:3000/expenses?ps=' + id + '&idps=' + idps );
           }
           getExpenseById(id: number ) {
            return this.http.get('http://localhost:3000/expenses?id=' + id );
             }
    // tslint:disable-next-line:max-line-length
    addExpenses( nom: string , ide: number , dsecription: string , ps: number , idps: number , idem: number , idb: number , idt: number , solde: number , idp: number , total: number , date: Date , req: string   ) {
      return this.http.post('http://localhost:3000/expenses', {
        nom: nom,
        ide: ide ,
        dsecription: dsecription,
        ps: ps ,
        idps: idps ,
        idem: idem ,
        idb: idb ,
        idt: idt ,
        solde: solde ,
        idp: idp ,
        total: total,
        date: date ,
        req: req
        }) ;
    }

    // tslint:disable-next-line:max-line-length
    patchExpenses( id: number , nom: string , ide: number , dsecription: string , ps: number , idps: number , idem: number , idb: number , idt: number , solde: number , idp: number , total: number , date: Date , req: string   ) {
      return this.http.patch('http://localhost:3000/expenses/' + id, {
        nom: nom,
        ide: ide ,
        dsecription: dsecription,
        ps: ps ,
        idps: idps ,
        idem: idem ,
        idb: idb ,
        idt: idt ,
        solde: solde ,
        idp: idp ,
        total: total,
        date: date ,
        req: req
        }) ;
    }

    deletExpenses(id: number ) {
      return this.http.delete('http://localhost:3000/expenses/' + id) ;
    }

}
