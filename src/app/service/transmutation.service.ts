import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' ;
import { NumberSymbol } from '@angular/common';
@Injectable({providedIn: 'root'})

export class TransmutationService {
  constructor (private http: HttpClient , ) {}

    getTrans() {
        return this.http.get('http://localhost:3000/transmutation');
         }
         getTransById(id: number ) {
          return this.http.get('http://localhost:3000/transmutation?id=' + id);
           }
    addTrans( nom1: string, num1: string , nom2: string , num2: string , perc: number  , solde: number , date: number,  ) {
      return this.http.post('http://localhost:3000/transmutation', {
        nom1: nom1,
        num1: num1,
        nom2: nom2,
        num2: num2,
        perc: perc,
        solde: solde,
        date: date

        }) ;
    }

}
