import { Permis} from '../user.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' ;
@Injectable({providedIn: 'root'})

export class PaymentService {
  constructor (private http: HttpClient , ) {}


         getpayment() {
          return this.http.get('http://localhost:3000/payment');
           }
           getpaymentById( id: number ) {
            return this.http.get('http://localhost:3000/payment?id=' + id);
             }
    addpayment( nom: string  ) {
      return this.http.post('http://localhost:3000/payment', {
        nom: nom
        }) ;
    }

}
