
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' ;
import {Paypal, Bank , Autre } from '../Tranfert.class';
@Injectable({providedIn: 'root'})

export class EmployeeService {
  constructor (private http: HttpClient , ) {}

    getEmployee() {
        return this.http.get('http://localhost:3000/employee');
         }
         getEmployeeById(id: any) {
          return this.http.get('http://localhost:3000/employee?id=' + id);
           }

    addEmployee1( nom: String, tel: string, email: String, ids: number , desc: String , idt: number  , paypal: Paypal ) {
      return this.http.post('http://localhost:3000/employee', {
        nom: nom,
        tel: tel,
        email: email,
        ids: ids,
        desc: desc,
        paypal: Paypal,
        }) ;
    }

    addEmployee2( nom: String, tel: string, email: String, ids: number , desc: String , idt: number  , autre: Autre ) {
      return this.http.post('http://localhost:3000/employee', {
        nom: nom,
        tel: tel,
        email: email,
        ids: ids,
        desc: desc,
        autre: Autre,
        }) ;
    }

    addEmployee3( nom: String, tel: string, email: String, ids: number , desc: String , idt: number  , bank: Bank ) {
      return this.http.post('http://localhost:3000/employee', {
        nom: nom,
        tel: tel,
        email: email,
        ids: ids,
        desc: desc,
        bank: Bank,
        }) ;
    }

}
