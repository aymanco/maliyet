import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' ;

@Injectable({providedIn: 'root'})

export class ExpenseService {
  constructor (private http: HttpClient) {}

    getexpense() {
        return this.http.get('http://localhost:3000/expense');
         }
         getExpenseById(id: number ) {
          return this.http.get('http://localhost:3000/expense?id=' + id);
           }
    addexpense(  nom: String  ) {
      return this.http.post('http://localhost:3000/expense', {
        nom: nom
        }) ;
    }

}
