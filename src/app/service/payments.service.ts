import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' ;
@Injectable({providedIn: 'root'})

export class PaymentsService {
  constructor (private http: HttpClient , ) {}


         getpayment() {
          return this.http.get('http://localhost:3000/payments');
           }

           getpaymentById(id: number ) {
            return this.http.get('http://localhost:3000/payments?id=' + id );
             }
             getpaymentByIdp(id: number ) {
              return this.http.get('http://localhost:3000/payments?idp=' + id );
               }
    // tslint:disable-next-line:max-line-length
    addpayment( idp: number , numd: number , solde: number , soldep: number ,  idtp: number , rest: number , bank: string , numb: number , bankch: string ,  datech: Date , numch: number ) {
      return this.http.post('http://localhost:3000/payments', {
        idp: idp,
        numd: numd,
        solde: solde,
        soldep: soldep,
        idtp: idtp,
        rest: rest,
        bank: bank,
        numb: numb,
        bankch: bankch,
        datech: datech,
        numch: numch
    });
  }

   // tslint:disable-next-line:max-line-length
   addpayment2( idp: number , numd: number , solde: number , soldep: number ,  idtp: number , rest: number , bank: string , numb: number , paypal: string ) {
    return this.http.post('http://localhost:3000/payments', {
      idp: idp,
      numd: numd,
      solde: solde,
      soldep: soldep,
      idtp: idtp,
      rest: rest,
      bank: bank,
      numb: numb,
      paypal: paypal
  });
}


 // tslint:disable-next-line:max-line-length
 addpayment3( idp: number , numd: number , solde: number , soldep: number ,  idtp: number , rest: number , bank: string , numb: number , nomk: string , date: Date) {
  return this.http.post('http://localhost:3000/payments', {
    idp: idp,
    numd: numd,
    solde: solde,
    soldep: soldep,
    idtp: idtp,
    rest: rest,
    bank: bank,
    numb: numb,
    nomk: nomk,
    date: date
});
}

 // tslint:disable-next-line:max-line-length
 addpayment4( idp: number , numd: number , solde: number , soldep: number ,  idtp: number , rest: number , bank: string , numb: number , autre: string) {
  return this.http.post('http://localhost:3000/payments', {
    idp: idp,
    numd: numd,
    solde: solde,
    soldep: soldep,
    idtp: idtp,
    rest: rest,
    bank: bank,
    numb: numb,
    autre: autre
});
}

deletPaymen(id: number) {
  return this.http.delete('http://localhost:3000/payments/' + id) ;
}



// tslint:disable-next-line:max-line-length
putpayment( id: number , idp: number , numd: number , solde: number , soldep: number ,  idtp: number , rest: number , bank: string , numb: number , bankch: string ,  datech: Date , numch: number ) {
  return this.http.put('http://localhost:3000/payments/' + id , {
    idp: idp,
    numd: numd,
    solde: solde,
    soldep: soldep,
    idtp: idtp,
    rest: rest,
    bank: bank,
    numb: numb,
    bankch: bankch,
    datech: datech,
    numch: numch
});
}

// tslint:disable-next-line:max-line-length
putpayment2( id: number , idp: number , numd: number , solde: number , soldep: number ,  idtp: number , rest: number , bank: string , numb: number , paypal: string ) {
return this.http.put('http://localhost:3000/payments/' + id , {
  idp: idp,
  numd: numd,
  solde: solde,
  soldep: soldep,
  idtp: idtp,
  rest: rest,
  bank: bank,
  numb: numb,
  paypal: paypal
});
}


// tslint:disable-next-line:max-line-length
putpayment3( id: number , idp: number , numd: number , solde: number , soldep: number ,  idtp: number , rest: number , bank: string , numb: number , nomk: string , date: Date) {
return this.http.put('http://localhost:3000/payments/' + id , {
idp: idp,
numd: numd,
solde: solde,
soldep: soldep,
idtp: idtp,
rest: rest,
bank: bank,
numb: numb,
nomk: nomk,
date: date
});
}

// tslint:disable-next-line:max-line-length
putpayment4(id: number , idp: number , numd: number , solde: number , soldep: number ,  idtp: number , rest: number , bank: string , numb: number , autre: string) {
return this.http.put('http://localhost:3000/payments/' + id, {
idp: idp,
numd: numd,
solde: solde,
soldep: soldep,
idtp: idtp,
rest: rest,
bank: bank,
numb: numb,
autre: autre
});
}
}
