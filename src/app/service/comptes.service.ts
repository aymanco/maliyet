import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' ;
@Injectable({providedIn: 'root'})

export class ComptesService {
  constructor (private http: HttpClient) {}

    getComptes() {
        return this.http.get('http://localhost:3000/comptes');
         }
         getComptesById(id: any) {
          return this.http.get('http://localhost:3000/comptes?id=' + id);
           }
    addComptes( nom: string, num: number , solde: string , iban: number , percentage: number , nomperc: string  ) {
      return this.http.post('http://localhost:3000/comptes', {
       nom: nom,
       num: num,
       solde: solde ,
       iban: iban,
       percentage: percentage,
       nomperc: nomperc
        }) ;
    }
    putComptes( id: number , solde: number   ) {
      return this.http.patch('http://localhost:3000/comptes/' + id,  {
        solde: solde
         });
    }


    addPercentage( nom: string , perc: number , req: string ) {
      return this.http.post('http://localhost:3000/percentage', {
        nom: nom,
        perc: perc,
        req: req
      });
    }
    getPercentage() {
      return this.http.get('http://localhost:3000/percentage');
       }
       getPercentageById(id: any) {
        return this.http.get('http://localhost:3000/percentage?id=' + id);
         }
         deletPercentageById(id: number) {
           return this.http.delete('http://localhost:3000/percentage/' + id) ;
         }


}
