
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' ;

@Injectable({providedIn: 'root'})

export class ServiceService {
  constructor (private http: HttpClient) {}

    getService() {
        return this.http.get('http://localhost:3000/service');
         }
         getServiceByID(id: number ) {
           return this.http.get('http://localhost:3000/service?id=' + id) ;
         }
    // tslint:disable-next-line:max-line-length
    addproject(  nom: String , dated: Date, datef: Date, solde: number  , dsecription: String , req: String ) {
      return this.http.post('http://localhost:3000/service', {
        nom: nom,
        dated: dated,
        datef: datef,
        solde: solde,
        dsecription: dsecription,
        req: req
        }) ;
    }

}
