import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http' ;
import { Costs } from '../cost.class';
import { Cost } from '../cost.model';

@Injectable({providedIn: 'root'})

export class ProjectService {
  constructor (private http: HttpClient) {}

    getproject() {
        return this.http.get('http://localhost:3000/project');
         }

    getprojectById(id: any  ) {
          return this.http.get('http://localhost:3000/project?id=' + id);
           }
    // tslint:disable-next-line:max-line-length
    addproject(  nom: String , dated: Date, datef: Date, dsecription: String, idc: Number, cost: Number , np: Number, p: Costs, req: String  ) {
      return this.http.post('http://localhost:3000/project', {
        nom: nom,
        dated: dated,
        datef: datef,
        dsecription: dsecription,
        idc: idc,
        cost: cost,
        np: np,
        p: p,
        req: req
        }) ;
    }

    patchProjec( id: number , p: Costs ) {
      return this.http.patch('http://localhost:3000/project/' + id, { p });
    }

}
