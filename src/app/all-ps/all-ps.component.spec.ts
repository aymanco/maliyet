import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPsComponent } from './all-ps.component';

describe('AllPsComponent', () => {
  let component: AllPsComponent;
  let fixture: ComponentFixture<AllPsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllPsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllPsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
