import { Component, OnInit } from '@angular/core';
import { ExpenseService } from '../service/expense.service';
import { ProjectService } from '../service/project.service';
import { ServiceService } from '../service/service.service';
import { EmployeeService } from '../service/employee.service';
import { ComptesService } from '../service/comptes.service';
import { PaymentService } from '../service/payment.service';
import { AddExpenseService } from '../service/addexpense.service';
import { UserService } from '../service/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-expense',
  templateUrl: './add-expense.component.html',
  styleUrls: ['./add-expense.component.css']
})
// tslint:disable-next-line:component-class-suffix

export class AddExpenseComponent implements OnInit {
  expenses: any ;
  projects: any ;
  services: any;
  employees: any ;
  comptes: any ;
  payments: any;
  persentages: any;
  total: number = null ;
  nisba: string ;
dis = true ;
  yes1 = false ;
  yes2 = false ;
  show: any ;
open = false ;
  i = 0;
  idps: number ;
  id: number;
  addsolde: any;
  idd: string;
  permi: any;
  add: boolean ;
  form: FormGroup ;
  // tslint:disable-next-line:max-line-length
  constructor(private expense: ExpenseService , private formBuilder: FormBuilder, private addexpense: AddExpenseService , private payment: PaymentService ,  private project: ProjectService , private compte: ComptesService , private service: ServiceService , private employee: EmployeeService , private user: UserService) {
  this.idd = localStorage.getItem('token');

  }

  async ngOnInit() {

     this.form = this.formBuilder.group({
        nom : [null, Validators.required],
        single1 : [null, Validators.required],
        single0 : [null, Validators.required],
        single2 : [null, Validators.required],
        single33 : [null, Validators.required],
        single3 : [null, Validators.required],
        single11 : [null, Validators.required],
        single10 : [null, Validators.required],
        date : [null, Validators.required]
    });


    await this.user.getUserByUser(this.idd).toPromise().then( data => {
      this.permi = data[0].permi.expenses ;
      this.add = this.permi.add ;
    } );

await this.expense.getexpense().toPromise().then( data => {
  this.expenses = data ;

});
await this.project.getproject().toPromise().then( data => {
  this.projects = data ;
});

await this.service.getService().toPromise().then( data => {
  this.services = data ;
});

await this.employee.getEmployee().toPromise().then( data => {
this.employees = data ;
});

await this.compte.getComptes().toPromise().then( data => {
this.comptes = data ;
});
 await this.payment.getpayment().toPromise().then( data => {
this.payments = data ;
 });

 await this.compte.getPercentage().toPromise().then( data => {
 this.persentages = data ;
 });


  }

  async solde( e ) {
    this.yes2 = true ;

    // tslint:disable-next-line:triple-equals
    if ( this.yes1 == true) {
  const id = e.target.value ;
  await this.compte.getPercentageById( id ).toPromise().then( data => {
    this.nisba = data[0].perc;
  } );

  const cost = (<HTMLInputElement>document.getElementById('single11')).value  ;
  // tslint:disable-next-line:radix
  this.total = parseInt(cost) +  (parseInt(cost) * parseInt(this.nisba ) / 100 );
    }
}

async solde1 ( e ) {
  this.yes1 = true;
  console.log(this.yes1);
  // tslint:disable-next-line:triple-equals
  if (this.yes2 == true ) {
  const cost = e.target.value ;
  const id =  (<HTMLInputElement>document.getElementById('single10')).value ;
  await this.compte.getPercentageById( id ).toPromise().then( data => {
    this.nisba = data[0].perc;
  } );

  // tslint:disable-next-line:radix
  this.total = parseInt(cost) +  (parseInt(cost) * parseInt(this.nisba ) / 100 );
  }

}

async Compte(e) {
  const id = e.target.value ;
  await this.employee.getEmployeeById(id).toPromise().then( data => {
  this.show = data[0] ;
  });
  this.open = true ;
}


 async Add(event) {
  const target = event.target ;
console.log(target);

const nom = target.querySelector('#nom').value ;
const ide = target.querySelector('#single1').value ;
const desc = target.querySelector('#desc').value ;
const ps = target.querySelector('#single0').value ;
const idem = target.querySelector('#single2').value ;
const idb = target.querySelector('#single33').value ;
const idt = target.querySelector('#single3').value ;
const solde = target.querySelector('#single11').value ;
const idp = target.querySelector('#single10').value ;
const total = target.querySelector('#total').value ;
const date = target.querySelector('#date').value ;
const req = target.querySelector('#req').value ;

this.id = ps % 10 ;
console.log('id : ', this.id);

 // tslint:disable-next-line:max-line-length
 this.addexpense.addExpenses(nom , ide , desc , this.id , Math.round( ps / 10), idem , idb , idt , solde , idp , total , date , req ).subscribe();

await this.compte.getComptesById(idb).toPromise().then( data => {
  this.addsolde = data[0].solde ;
});

 // tslint:disable-next-line:radix
 this.compte.putComptes( idb , Math.round(parseInt( this.addsolde) - parseInt(total) ) ).subscribe();

 }

ok(e) {
  console.log(e.target.value);
}

}
