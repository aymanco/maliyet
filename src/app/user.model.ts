
 export interface User {
      idu: number;
      userName: String;
      emial: String;
      password: String;
      description: String;
      permis: Permis;
}

// tslint:disable-next-line:class-name
export interface Permis {
      projet: Projets;
      service: Services;
      expenses: Expenses;
      payments: Payments;
      setting: Setting;
      users: Users;
}
// tslint:disable-next-line:class-name
export interface Projets {

  add: boolean ;
  edit: boolean ;
  deletes: boolean ;
  poste: boolean ;
  up: boolean ;
}
// tslint:disable-next-line:class-name
export interface Services {

  add: boolean ;
  edit: boolean ;
  deletes: boolean ;
  poste: boolean ;
  up: boolean ;
}
// tslint:disable-next-line:class-name
export interface Expenses {

  add: boolean ;
  edit: boolean ;
  deletes: boolean ;
  poste: boolean ;
  up1: boolean ;
  up2: boolean ;
}
// tslint:disable-next-line:class-name
export interface Payments {

  add: boolean ;
  edit: boolean ;
  deletes: boolean ;
  poste: boolean ;
  up1: boolean ;
  up2: boolean ;
}
// tslint:disable-next-line:class-name
export interface Setting {

  addG: boolean ;
  addp: boolean ;
  addS: boolean ;
  edit: boolean ;
  change: boolean ;
  add: boolean;
}
// tslint:disable-next-line:class-name
export interface Users {

  addU: boolean ;
  addC: boolean ;
  addS: boolean ;
  editu: boolean ;
  editC: boolean ;
  editS: boolean ;
  deleteU: boolean;
  deleteC: boolean;
  deleteS: boolean;
}
