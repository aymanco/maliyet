import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from '../service/service.service';
import { AddExpenseService } from '../service/addexpense.service';

@Component({
  selector: 'app-service-details',
  templateUrl: './service-details.component.html',
  styleUrls: ['./service-details.component.css']
})
export class ServiceDetailsComponent implements OnInit {
  id: number ;
  services: any;
  total = 0 ;
  expenses: any ;
  constructor( private route: ActivatedRoute , private service: ServiceService, private expense: AddExpenseService) { }

  async ngOnInit() {

    this.route.params.subscribe( params  => {
      this.id = params.id ;
    });
    await this.service.getServiceByID(this.id).toPromise().then(data => {
      this.services = data[0];
    });

    await this.expense.getExpenseByIdpsIdp(2 , this.id ).toPromise().then(data => {
      this.expenses = data ;
    });

    for (const tab of this.expenses ) {
      // tslint:disable-next-line:radix
      Math.round( this.total += parseInt( tab.solde ));
    }
    console.log(this.total);


  }

}
