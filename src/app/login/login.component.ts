import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { UserService } from '../service/user.service';
import { AppComponent } from '../app.component';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  model:  any ;
  loginForm: FormGroup;
  message: string;
  returnUrl: string;
  mawjod = false ;
  i = 0 ;
  indice = 0 ;
  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder, private router: Router, public authService: AuthService , private users: UserService) { }

  async ngOnInit() {
    this.loginForm = this.formBuilder.group({
      userid: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.returnUrl = '/board';
    this.authService.logout();


await this.users.getUser().toPromise().then( data => {
  this.model = data ;
  console.log(data) ;
});
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  async login() {

    this.i = 0 ;
    this.indice = 0 ;
for ( const user of this.model ) {
  // tslint:disable-next-line:triple-equals
  if ( user.userName == this.f.userid.value  ) { this.mawjod = true ; this.indice = this.i ; }
  this.i++ ;


}
    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    } else {
      if ( this.mawjod ) {
      // tslint:disable-next-line:triple-equals
      if (this.f.userid.value == this.model[this.indice].userName && this.f.password.value == this.model[this.indice].password ) {
        console.log('Login successful');
        // this.authService.authLogin(this.model);
        localStorage.setItem('isLoggedIn', 'true');
        localStorage.setItem('token', this.f.userid.value);
        this.router.navigate([this.returnUrl]);
      } else {
        this.message = 'Please check your userid and password';
      }
    } else {
      this.message = 'Please check your userid and password';
  }
}

}

}
