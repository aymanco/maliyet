import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PaymentsService } from '../service/payments.service';
import { ProjectService } from '../service/project.service';
import { Payment } from '../payments.class';
import { ClientService } from '../service/client.service';
import { PaymentService } from '../service/payment.service';
import { ComptesService } from '../service/comptes.service';
import { TransmutationService } from '../service/transmutation.service';
import * as jsPDF from 'jspdf' ;
import * as html2canvas from 'html2canvas';
import { delay } from 'q';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-all-payments',
  templateUrl: './all-payments.component.html',
  styleUrls: ['./all-payments.component.css']
})
export class AllPaymentsComponent implements OnInit {
 @ViewChild('content') content: ElementRef ;

paymants: any ;
look = new Payment() ;
i = 0 ;
nom = [];
clients = [] ;
projet: any ;
clienShow: any ;
id: number ;
model_trans: any  ;
bank_trans: any ;
open = false;
allprojects: object;
paymentModif: any;
modife = false;
tab = Array(0) ;
numd = 0;
valeur: any ;
vape: any;
dofaa: number ;
dofa: any ;
p: any ;
payments: any ;
comptes: any;
val = 'none';
  val2 = 'none';
  val3 = 'none';
  val4 = 'none';
  dofas: any ;
  rest = null ;
  num: any;
  idd: string ;
  permi: any;

// tslint:disable-next-line:max-line-length
constructor( private payment: PaymentsService , private transmutationService: TransmutationService, private projects: ProjectService , private client: ClientService , private putpayment: PaymentsService  , private paymentService: PaymentService , private compte: ComptesService, private user: UserService ) {
  this.idd = localStorage.getItem('token');

  this.projects.getproject().toPromise().then( data => {
    this.allprojects = data ;
  }) ;
}

  async ngOnInit() {

    await this.user.getUserByUser(this.idd).toPromise().then( data => { this.permi = data[0].permi.payments ; } );
    this.methode() ;

  }

  async methode() {
    await  this.payment.getpayment().toPromise().then( data => {
      this.paymants = data ;
    });
    this.i = 0;
    this.open = false ;

    for ( const project of this.paymants ) {
      this.i ++ ;
         await this.projects.getprojectById(project.idp).toPromise().then( async data => {
           this.nom[this.i - 1 ] = data[0].nom ;
           await this.client.getCLientById( data[0].idc ).toPromise().then( donnee => {
            this.clients[this.i - 1] = donnee[0].nom ;
           });
         });
    }
  }

  async affiche(e) {
    this.id = e ;
   await this.payment.getpaymentById( e ).toPromise().then( data => {
       this.look = data[0] ;
       console.log(this.look);
    } );

    const x: number = this.look.idp ;
    await this.projects.getprojectById(x).toPromise().then( donnee => {
      this.projet = donnee[0] ;
      console.log(this.projet) ;
    } );

    await this.client.getCLientById(this.projet.idc).toPromise().then( yes => {
    this.clienShow = yes[0];
      console.log(this.clienShow) ;
    } );

    await this.paymentService.getpaymentById(this.look.idtp).toPromise().then( model => {
     this.model_trans = model[0] ;
      console.log(this.model_trans) ;
    } );

    await this.compte.getComptesById(this.look.bank).toPromise().then( banka => {
    this.bank_trans = banka[0] ;
      console.log(this.bank_trans) ;
    } );

this.open = true ;

  }
  delet(e) {
    console.log(e) ;
    this.id = e ;


  }

  async deletPayment() {
    console.log(this.id);
    await this.payment.deletPaymen( this.id ).toPromise().then();
     this.methode();
  }

  async modif ( val ) {
    this.valeur = val.solde ;
    this.p = val ;
    console.log(val) ;
   await this.projects.getprojectById(val.idp).toPromise().then( data => {
   this.paymentModif = data[0] ;
   this.vape = data[0].p ;
   });
   this.numd = val.numd ;
   console.log(this.numd);
   // tslint:disable-next-line:radix
   this.tab = Array(parseInt(this.paymentModif.np));
   this.modife = true ;
    // tslint:disable-next-line:triple-equals
  if ( this.numd == 1 ) { this.dofaa = this.vape.const1 ; }
  // tslint:disable-next-line:triple-equals
  if ( this.numd == 2 ) { this.dofaa = this.vape.const2 ; }
  // tslint:disable-next-line:triple-equals
  if ( this.numd == 3 ) { this.dofaa = this.vape.const3 ; }
  // tslint:disable-next-line:triple-equals
  if ( this.numd == 4 ) { this.dofaa = this.vape.const4 ; }

  await this.paymentService.getpayment().toPromise().then(data => {
this.payments = data ;
  });
  await this.compte.getComptes().toPromise().then(data => {
    this.comptes = data ;
  });

  // tslint:disable-next-line:triple-equals
  if ( val.idtp == 1  ) {
    this.val = 'block' ;
     this.val2 = 'none' ;
     this.val3 = 'none' ;
      this.val4 = 'none';
    // tslint:disable-next-line:triple-equals
    } else if ( val.idtp == 2  ) {
      this.val = 'none' ;
       this.val2 = 'block' ;
        this.val3 = 'none' ;
         this.val4 = 'none' ;
         // tslint:disable-next-line:triple-equals
         } else if ( val.idtp == 3  ) {
           this.val = 'none' ;
            this.val2 = 'none' ;
             this.val3 = 'block'
             ; this.val4 = 'none';
             } else {
                this.val = 'none' ;
                this.val2 = 'none' ;
                this.val3 = 'none' ;
                this.val4 = 'block' ;
                 }
                 this.dofas = this.vape ;
                 this.rest = val.rest ;
                 this.num = val.numb ;
  }

  async change(e) {
    this.dofa = null ;
   await this.projects.getprojectById(e.target.value).toPromise().then(data => {
      this.dofa = data[0].np ;
      this.dofas = data[0].p ;
    }) ;
    // tslint:disable-next-line:radix
    this.tab = Array(parseInt(this.dofa));
    console.log(this.dofa);
    this.valeur = 0 ;
  }


  cost(e) {
    const x = e.target.value;
  // tslint:disable-next-line:triple-equals
  if ( x == 1 ) { this.valeur = this.dofas.const1 ; }
  // tslint:disable-next-line:triple-equals
  if ( x == 2 ) { this.valeur = this.dofas.const2 ; }
  // tslint:disable-next-line:triple-equals
  if ( x == 3 ) { this.valeur = this.dofas.const3 ; }
  // tslint:disable-next-line:triple-equals
  if ( x == 4 ) { this.valeur = this.dofas.const4 ; }
  console.log((<HTMLInputElement>document.getElementById('money')).value );

  // tslint:disable-next-line:radix
  const  money = parseInt( (<HTMLInputElement>document.getElementById('money')).value ) ;

  // tslint:disable-next-line:triple-equals
  if ( money  != 0 ) {
  console.log(this.valeur);
  this.Rest1( money , this.valeur );
  }

  }

  Dafea(e) {
    const x = e.target.value ;
    // tslint:disable-next-line:triple-equals
    if ( x == 1  ) {
      this.val = 'block' ;
       this.val2 = 'none' ;
       this.val3 = 'none' ;
        this.val4 = 'none';
      // tslint:disable-next-line:triple-equals
      } else if ( x == 2  ) {
        this.val = 'none' ;
         this.val2 = 'block' ;
          this.val3 = 'none' ;
           this.val4 = 'none' ;
           // tslint:disable-next-line:triple-equals
           } else if ( x == 3  ) {
             this.val = 'none' ;
              this.val2 = 'none' ;
               this.val3 = 'block'
               ; this.val4 = 'none';
               } else {
                  this.val = 'none' ;
                  this.val2 = 'none' ;
                  this.val3 = 'none' ;
                  this.val4 = 'block' ;
                   }
  }

  Rest(e) {
    const solde =  (<HTMLInputElement>document.getElementById('solde')).value ;
    // tslint:disable-next-line:radix
    this.rest = parseInt (solde) - e.target.value ;
  }
  Rest1(e , x ) {
    this.rest =   x - e   ;
  }

  async NumB(e) {
    const x = e.target.value ;
    await this.compte.getComptesById(x).toPromise().then( data => {
    this.num = data[0].num ;
    });
    console.log(this.num);
    }


    Putpayment(event) {
    const target = event.target ;
    const idp = target.querySelector('#single0').value;
    const numd = target.querySelector('#single1').value;
    const solde = target.querySelector('#solde').value;
    const soldep = target.querySelector('#money').value;
    const idtp = target.querySelector('#payment-type').value;
    const rest = target.querySelector('#rest').value;
    const bank = target.querySelector('#single4').value;
    const numb = target.querySelector('#num').value;
    console.log('hhh : ', idtp );
    console.log(target.querySelector('#paypalm').value);

    // tslint:disable-next-line:triple-equals
    if ( idtp == 3  ) {
    const bankch = target.querySelector('#bank').value;
    const datech = target.querySelector('#date').value;
    const numch = target.querySelector('#numch').value;
    this.putpayment.putpayment(this.p.id , idp , numd , solde , soldep , idtp , rest , bank , numb , bankch , datech ,  numch ).subscribe();

    // tslint:disable-next-line:triple-equals
    } else if ( idtp == 1 ) {
    const paypal = target.querySelector('#paypalm').value;
    this.putpayment.putpayment2(this.p.id , idp , numd , solde , soldep , idtp , rest , bank , numb , paypal  ).subscribe();


    // tslint:disable-next-line:triple-equals
    } else if ( idtp == 2 ) {
    const nomk = target.querySelector('#nomk').value;
    const date = target.querySelector('#datek').value;
    this.putpayment.putpayment3( this.p.id , idp , numd , solde , soldep , idtp , rest , bank , numb , nomk , date  ).subscribe();

    } else {
    const autre = target.querySelector('#autre').value ;
    this.putpayment.putpayment4( this.p.id , idp , numd , solde , soldep , idtp , rest , bank , numb ,  autre ).subscribe();

    }



  }

  async downloadPDF(id) {
    await this.affiche(id).then() ;
    await delay(1000) ;
    this.open = true ;
    /* const doc = new jsPDF();

    const specialElmentHandLers = {
     '#editor' : function (element , renderer) {
       return true ;
     }
    };
    const content = this.content.nativeElement ;
    doc.setFont('Helvetica') ;
    console.log(doc.getFontList());
    doc.fromHTML(content.innerHTML , 15, 15 , {
    'width': 190 ,
    'elementHandLers': specialElmentHandLers ,
    'lang': 'ar'
    });
    doc.save('maliyet.pdf');
 */
 /*  html2canvas(document.getElementById('show')).then(function(canvas) {

      const img = canvas.toDataURL('image/png');
    const doc = new jsPDF();
    doc.addImage(img, 'JPEG', 5, 0);
    doc.save('testCanvas.pdf');
    }); */

    const data = document.getElementById('show');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      const imgWidth = 208;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      const contentDataURL = canvas.toDataURL('image/png');
      const pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
      const position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
      pdf.save('MYPdf.pdf'); // Generated PDF
    });

  }
}
