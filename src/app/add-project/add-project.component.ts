import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {FormControl} from '@angular/forms';
import {ClientService} from '../service/client.service' ;
import { Costs } from '../cost.class';
import { ProjectService } from '../service/project.service';
import { UserService } from '../service/user.service';


@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
export class AddProjectComponent implements OnInit , AfterViewInit {
  profil: any;
   go  = 'none';
  go2 = 'none' ;
  go3 = 'none' ;
  go4 = 'none' ;
  y = false ;
  y1 = false ;
  public x = 0 ;
  clients: object;
  cost = new Costs();
  date = new FormControl();
  date1 = new FormControl();
  date2 = new FormControl();
  form: FormGroup ;
  id: string ;
  add: boolean ;
  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  }



  public loadScript21() {  const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/select2/js/select2.full.min.js' ;
    body.appendChild(jquery);
  }
  public loadScript22() {  const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' ;
    body.appendChild(jquery);
  }
  public loadScript23() {  const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/pages/scripts/components-date-time-pickers.min.js' ;
    body.appendChild(jquery);
  }
  public loadScript24() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/pages/scripts/components-select2.min.js' ;
    body.appendChild(jquery);
  }
  public loadScript25() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/js/js.js' ;
    body.appendChild(jquery);
  }
  public loadScript26() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/scripts/app.min.js' ;
    body.appendChild(jquery);
  }
  public loadScript27() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ar.min.js' ;
    body.appendChild(jquery);
  }
  public loadScript28() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js' ;
    body.appendChild(jquery);
  }
  public loadScript29() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js' ;
    body.appendChild(jquery);
  }
  public loadScript30() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/moment.min.js' ;
    body.appendChild(jquery);
  }
  public loadScript31() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' ;
    body.appendChild(jquery);
  }
  public loadScript32() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/uniform/jquery.uniform.min.js' ;
    body.appendChild(jquery);
  }
  public loadScript33() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/jquery.blockui.min.js' ;
    body.appendChild(jquery);
  }

   public loadScript34() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' ;
    body.appendChild(jquery);
  }
  public loadScript35() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js' ;
    body.appendChild(jquery);
  }
  public loadScript36() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/js.cookie.min.js' ;
    body.appendChild(jquery);
  }
  public loadScript37() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/bootstrap/js/bootstrap.min.js' ;
    body.appendChild(jquery);
  }
  public loadScript38() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/global/plugins/jquery.min.js' ;
    body.appendChild(jquery);
  }
   public loadScript39() {
    const body = <HTMLDivElement> document.body;
    const jquery = document.createElement('script');
    jquery.type = 'text/javascript';
    jquery.src = '../../assets/js/js.js' ;
    body.appendChild(jquery);
  }
  // tslint:disable-next-line:max-line-length
  constructor(public clientService: ClientService , public projectService: ProjectService, private formBuilder: FormBuilder, private user: UserService) {
    this.id = localStorage.getItem('token');
    this.loadScript21();
    this.loadScript22();
    this.loadScript23();
    this.loadScript24();
    this.loadScript25();
    this.loadScript26();
    this.loadScript27();
    this.loadScript28();
    this.loadScript29();
    this.loadScript30();
    this.loadScript31();
    this.loadScript32();
    this.loadScript33();
    this.loadScript34();
    this.loadScript35();
    this.loadScript36();
    this.loadScript37();



   }
   private loadScript(scriptUrl: string) {
    return new Promise((resolve, reject) => {
    const scriptElement = document.createElement('script');
    scriptElement.src = scriptUrl;
    scriptElement.onload = resolve;
    document.body.appendChild(scriptElement);
   });
  }
  async ngAfterViewInit() {
    await this.loadScript('../../assets/js/js.js');

  }
  click(event) {
    const target = event.target ;
    this.x = target.value ;
    // tslint:disable-next-line:triple-equals
    if ( this.x == 1 ) {
       this.go = 'block' ;
       this.go2 = 'none' ;
       this.go3 = 'none' ;
       this.go4 = 'none' ;
    // tslint:disable-next-line:triple-equals
    } else if ( this.x == 2 ) {
      this.go = 'block' ;
      this.go2 = 'block' ;
      this.go3 = 'none' ;
      this.go4 = 'none' ;
    // tslint:disable-next-line:triple-equals
    } else if ( this.x == 3 ) {
    this.go = 'block' ;
    this.go2 = 'block' ;
    this.go3 = 'block' ;
    this.go4 = 'none' ;
    // tslint:disable-next-line:triple-equals
  } else if ( this.x == 4 ) {
    this.go = 'block' ;
    this.go2 = 'block' ;
    this.go3 = 'block' ;
    this.go4 = 'block' ;
    // tslint:disable-next-line:triple-equals
  } else if ( this.x == 0 ) {
    this.go = 'none' ;
    this.go2 = 'none' ;
    this.go3 = 'none' ;
    this.go4 = 'none' ;
  }
  }

  submit(event) {
    const target = event.target ;
    const nom = target.querySelector('#nom').value ;
    const dated = target.querySelector('#dated').value ;
    const datef = target.querySelector('#datef').value ;
    const desc = target.querySelector('#desc').value ;
    const single = target.querySelector('#single').value ;
    const cost = target.querySelector('#cost').value ;

     const const1 = target.querySelector('#cost1').value ;
     const dconst1 = target.querySelector('#dcost1').value ;

     const const2 = target.querySelector('#cost2').value;
     const dconst2 = target.querySelector('#dcost2').value;

     const const3 = target.querySelector('#cost3').value;
     const dconst3 = target.querySelector('#dcost3').value;

     const const4 = target.querySelector('#cost4').value;
     const dconst4 = target.querySelector('#dcost4').value;

    if (this.x >= 1 ) {
      this.cost.const1 = const1 ;
      this.cost.dconst1 = dconst1 ;
      this.cost.etat1 = false ;
      if (this.x >= 2 ) {
        this.cost.const2 = const2 ;
        this.cost.dconst2 = dconst2 ;
        this.cost.etat2 = false ;

        if ( this.x >= 3 ) {
          this.cost.const3 = const3 ;
          this.cost.dconst3 = dconst3 ;
          this.cost.etat3 = false ;

        }
    // tslint:disable-next-line:triple-equals
        if (this.x == 4) {
          this.cost.const4 = const4 ;
          this.cost.dconst4 = dconst4 ;
          this.cost.etat4 = false ;

        }
       }
    }
    const req = target.querySelector('#req').value ;

 this.projectService.addproject(nom, dated, datef, desc, single, cost, this.x, this.cost, req).subscribe( data => {
  console.log(data);
} );
  }

  async ngOnInit() {

    this.clientService.getCLient().subscribe( data => {
      this.clients = data ;
      console.log(data);
    });

    this.form = this.formBuilder.group({
      nom : [null, Validators.required],
      client : [null, Validators.required],
      cost : [null, Validators.required],
      npayment : [null, Validators.required]

  });


  await this.user.getUserByUser(this.id).toPromise().then( data => {
    this.profil = data[0].permi.projet ;
  } );
  console.log(this.profil);
  this.add = this.profil.add ;
  console.log(this.add) ;
  }
  test(event) {
const target = event.target ;
console.log('hi');
if ( target.value != null ) { this.y = true ; }
  }
  test1(event) {

    const target = event.target ;
    console.log(target.value);
    if ( target.value != null ) { this.y1 = true ; }
  }
}
